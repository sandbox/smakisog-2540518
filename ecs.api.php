<?php

/**
 * @file
 * Hooks provided by ECS module.
 */

/**
 * Alter send packet.
 *
 * @param ECSSyncPacket $packet
 *   The packet object.
 */
function hook_ecs_send_alter(ECSSyncPacket $packet) {
  $packet->bundle = 'news';
  $packet->fields['is_new'] = 1;
  $packet->fields['title'] = '........';
}

/**
 * Alter receive packet.
 *
 * @param ECSSyncPacket $packet
 *   The packet object.
 */
function hook_ecs_receive_alter(ECSSyncPacket $packet) {
  $packet->bundle = 'blog';
  $packet->fields['is_new'] = 0;
  $packet->fields['title'] = '........';
}
