------------
- ECS module
------------

DESCRIPTION:
------------

Synchronize entity by chosen content types between two
Drupal installations with Rules (http://drupal.org/project/rules).

REQUIREMENTS
------------

This module requires the following modules:
  - List (part of Drupal 7 core Field module)
  - Entity API (https://www.drupal.org/project/entity)
  - Rules (https://www.drupal.org/project/rules)

INSTALLATION:
-------------

1. Install the module as usual, see http://drupal.org/node/70151 for further
 information.

2. Enable the ecs module by navigating to:

     administer > modules

CONFIGURE:
----------
1. Goto ECS settings page (admin/config/system/ecs) and
- Choose which node types to sync
- Set remore server path
- Enter secret key

2. After saving settings push Create Alias Map for first time initialization of
 nodes relations between sites (module must be enabled on both sites)

3. If you want synchronize all chosen content which present on current site,
 then goto Sync all checked contents page (admin/config/system/ecs/ecs_batch)
 and Run sync process

**Credits:
The original module is significant enhancements of Teemu Merikoski's Entity Sync
module (https://www.drupal.org/project/entitysync)
made by Igor Kozhanov (smakisog)

Current maintainers:
* Igor Kozhanov (smakisog) - http://www.drupal.org/user/1508964
* Eugen Cherkashin (biman) - http://www.drupal.org/user/1565710
* Stanislav Proshkin (s_proshkin) - http://drupal.org/user/3270966

This project has been sponsored by:
* WDG - Wise Decision Group
  WDG Company offers a full range of Web development services to create,
  support, and promotion of Web solutions for business and individuals.
  Visit http://wdg-company.com for more information.
