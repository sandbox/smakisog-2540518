<?php
/**
 * @file
 * Definition of ECSPacket class.
 */

/**
 * Class ECSSyncPacket. Code and encode entity to packets.
 */
class ECSSyncPacket {

  /**
   * Unique field identifier (nid, fied_name, etc.).
   */
  public $uniqueField;

  /**
   * Unique field value.
   */
  public $uniqueValue;

  /**
   * Entity type.
   */
  public $entityType;

  /**
   * Entity bundle.
   */
  public $bundle;

  /**
   * Entity fields, which will be synchronized.
   */
  public $fields;

  /**
   * An base-64 encoded, URL-safe sha-256 hmac checksum for this packet.
   */
  public $checksum;

  /**
   * Operation, which will be made with packet (sync|del).
   */
  public $op;

  /**
   * Constructor.
   *
   * When both parameters are given, a packet is created from the given entity
   * using the unique field as a key.
   *
   * @param object $entity
   *   DrupalEntityWrapper object.
   * @param object|bool $unique
   *   Uniquie identifier (en nid, title, username).
   * @param array $fields
   *   Array of fields to sync.
   */
  public function __construct($entity = NULL, $unique = NULL, $fields = array()) {

    if ($entity == NULL) {
      return;
    }

    $info        = $unique->info();
    $field_name  = $info['name'];
    $entity_type = $entity->type();
    $bundle      = $entity->getBundle();

    // Prepare syncing.
    $this->uniqueField = $field_name;
    $this->uniqueValue = $unique->value();

    $this->entityType  = $entity_type;
    $this->bundle      = $bundle;
    $this->fields      = array();

    // Collect filtered fields.
    $filter_fields = array();
    $entity_info = $entity->entityInfo();

    foreach ($entity_info['entity keys'] as $id => $key) {
      if ($id != 'label') {
        $filter_fields[] = $key;
      }
    }

    // Collect the whole entity.
    if (isset($fields['-all-'])) {
      $fields = $entity->getPropertyInfo();
      $fields = array_keys($fields);
    }

    $entity_raw = $entity->raw();
    $field_handlers = ecs_get_field_handlers();

    foreach ($fields as $field_name) {
      if (!in_array($field_name, $filter_fields) && isset($entity_raw->$field_name)) {
        $info = field_info_field($field_name);

        if (isset($field_handlers[$info['type']])) {
          $this->fields[$field_name] = $field_handlers[$info['type']]['out']($entity->$field_name, $field_name, $entity_raw->$field_name);
        }
        else {
          // Other fields.
          $this->fields[$field_name] = $entity_raw->$field_name;
        }
      }
    }
  }

  /**
   * Decode a json object to this packet.
   *
   * @return bool
   *   TRUE or FALSE signifying if the packet is valid (trustworthy) or not.
   */
  public function decode($json) {
    $obj = drupal_json_decode($json);

    if (empty($obj)) {
      return FALSE;
    }

    foreach ($obj as $key => $value) {
      $this->$key = $value;
    }
    return $this->validate();
  }

  /**
   * Get encoded version of this packet.
   *
   * @return string
   *   Encoded packet.
   */
  public function encoded() {
    $this->checksum = $this->generateChecksum();
    return drupal_json_encode($this);
  }

  /**
   * Generate checksum for this packet.
   *
   * @return string
   *   An base-64 encoded, URL-safe sha-256 hmac checksum for this packet.
   */
  private function generateChecksum() {
    return drupal_hmac_base64(serialize($this->fields) . $this->getQueueIdentifier(), variable_get('ecs_shared_secret', substr(md5(rand()), 0, 7)));
  }

  /**
   * Fetch a Drupal entity for this packet.
   *
   * @return object|bool
   *   Drupal entity object or FALSE in case of an error.
   */
  public function fetchEntity() {
    $entity_field_query = new EntityFieldQuery();
    $entity_field_query->entityCondition('entity_type', $this->entityType)
      ->propertyCondition($this->uniqueField, $this->getRelatedNid());
    $entities = $entity_field_query->execute();

    // Check if node exist localy.
    if (isset($entities[$this->entityType]) && !empty($entities[$this->entityType])) {
      $ids = array_keys($entities[$this->entityType]);
      $number_of_entities = count($ids);
    }
    else {
      $number_of_entities = 0;
    }

    if ($number_of_entities > 0) {
      $entities = entity_load($this->entityType, $ids);
      $number_of_entities = count($entities);
    }

    if ($number_of_entities == 0) {
      // Create a new one.
      $info = entity_get_info($this->entityType);
      $bundle = $info['entity keys']['bundle'];
      $new = (object) NULL;
      $new->is_new = TRUE;
      if (!empty($bundle)) {
        $new->$bundle = $this->bundle;
      }
      return $new;
    }
    elseif ($number_of_entities == 1) {
      // Update existing.
      return array_shift($entities);
    }
    // Generate error.
    return FALSE;
  }

  /**
   * Validate the packet.
   *
   * @return bool
   *   TRUE if the packet is OK, FALSE if not.
   */
  protected function validate() {
    return $this->checksum === $this->generateChecksum();
  }

  /**
   * Validate checksum for this simple packet.
   *
   * @return string
   *   An base-64 encoded, URL-safe sha-256 hmac checksum for this packet.
   */
  static public function checksumValidate($checksum = NULL) {
    if (is_null($checksum)) {
      return $this->checksum === drupal_hmac_base64(variable_get('ecs_shared_secret', substr(md5(rand()), 0, 7)), variable_get('ecs_shared_secret', substr(md5(rand()), 0, 7)));
    }
    else {
      return $checksum === drupal_hmac_base64(variable_get('ecs_shared_secret', substr(md5(rand()), 0, 7)), variable_get('ecs_shared_secret', substr(md5(rand()), 0, 7)));
    }
  }

  /**
   * Get the queue identifier for this packet.
   *
   * @return string
   *   String which identifies the entity.
   */
  public function getQueueIdentifier() {
    return $this->entityType . ':'
      . $this->bundle . ':'
      . $this->uniqueField . ':'
      . $this->uniqueValue;
  }

  /**
   * Get entity local id by remote id.
   *
   * @return int|bool
   *   Etity id if exist, FALSE if not.
   */
  protected function getRelatedNid() {
    $local_query = db_select('ecs_reference_map', 'map')
      ->fields('map', array('local_nid'))
      ->condition('map.remote_nid', $this->uniqueValue)
      ->execute();
    $local = $local_query->fetch();

    return (isset($local->local_nid) && is_numeric($local->local_nid)) ? $local->local_nid : $this->uniqueValue;
  }

}
