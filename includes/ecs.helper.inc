<?php
/**
 * @file
 * Helpers function for ECS.
 */

/**
 * Create array of types which will be synced.
 *
 * @return array
 *   New alias map.
 */
function ecs_create_array() {
  $types = variable_get('ecs_sync_node_types', array());
  $array_map = array();

  if (!empty($types)) {
    foreach ($types as $key => $value) {
      $array_map[$key] = ecs_load_nodes_by_type($key);
    }
  }
  return $array_map;
}

/**
 * Load nodes by given type.
 *
 * @param string $type
 *   Type of node to retrieve.
 *
 * @return array
 *   Nodes by given type.
 */
function ecs_load_nodes_by_type($type) {
  $db_nodes_query = db_select('node', 'n')
    ->fields('n', array('nid', 'title', 'type'))
    ->condition('n.type', $type)
    ->execute();

  return $db_nodes_query->fetchAllKeyed();
}

/**
 * Compare two sites on contnet and create content mapping array.
 *
 * @return bool
 *   TRUE if map create successful or FALSE.
 */
function ecs_compare_content() {
  $comparsion_map = $comparsion_title_map = array();

  // Get remote map.
  $url = variable_get('ecs_remore_server', 'http://');
  $endpoint = $url . '/content_mapping';

  $data = array(
    'checksum' => drupal_hmac_base64(variable_get('ecs_shared_secret', substr(md5(rand()), 0, 7)), variable_get('ecs_shared_secret', substr(md5(rand()), 0, 7))),
  );

  $options = array(
    'method' => 'POST',
    'data' => drupal_http_build_query($data),
    'timeout' => 15,
    'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
  );

  $remote_map_request = drupal_http_request(trim($endpoint), $options);

  if (isset($remote_map_request->data) && $remote_map_request->status_message == 'OK') {
    $remote_map = json_decode($remote_map_request->data, 1);

    if (!is_array($remote_map)) {
      return FALSE;
    }

    // Step 1 - compare content map of the sites.
    $remote_duplicates = ecs_find_map_duplicates($remote_map, 1);
    $local_map = ecs_create_array();
    $local_duplicates = ecs_find_map_duplicates($local_map, 1);

    if (count($remote_duplicates) > 0) {
      // Step 2 - load all duplicate nodes objects.
      $endpoint = $url . '/get_remote_nodes';
      $data = array(
        'nids' => $remote_duplicates,
        'checksum' => drupal_hmac_base64(variable_get('ecs_shared_secret', substr(md5(rand()), 0, 7)), variable_get('ecs_shared_secret', substr(md5(rand()), 0, 7))),
      );

      $options = array(
        'method' => 'POST',
        'data' => drupal_http_build_query($data),
        'timeout' => 15,
        'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
      );

      $remote_nodes = drupal_http_request(trim($endpoint), $options);

      if (isset($remote_nodes->data) && $remote_nodes->status_message == 'OK') {
        $remote_nodes_array = json_decode($remote_nodes->data, 1);
        $local_nids_cheched = $remote_nids_cheched = array();

        foreach ($local_duplicates as $duplicate) {
          foreach ($remote_nodes_array as $remote_node) {
            $remote_node = (object) $remote_node;

            if (ecs_node_compare($duplicate, $remote_node)) {
              $comparsion_map[] = array(
                'title' => $remote_node->title,
                'local_nid' => $duplicate,
                'remote_nid' => $remote_node->nid,
              );

              $comparsion_title_map[] = $remote_node->title;
              $local_nids_cheched[] = $duplicate;
              $remote_nids_cheched[] = $remote_node->nid;
              break;
            }
          }
        }

        $local_notsinced_nids = array_diff($local_duplicates, $local_nids_cheched);
        $remote_notsinced_nids = array_diff($remote_duplicates, $remote_nids_cheched);

        if (count($local_notsinced_nids) > 0) {
          $local_notsinced_nodes = ecs_load_nodes_array($local_notsinced_nids);

          foreach ($local_notsinced_nodes as $lnn) {
            $comparsion_map[] = array(
              'title' => $lnn->title,
              'local_nid' => (int) $lnn->nid,
              'remote_nid' => NULL,
            );
            $comparsion_title_map[] = $lnn->title;
          }
        }

        if (count($remote_notsinced_nids) > 0) {
          foreach ($remote_notsinced_nids as $rnn) {
            if (isset($remote_nodes_array[$rnn])) {
              $comparsion_map[] = array(
                'title' => $remote_nodes_array[$rnn]['title'],
                'local_nid' => NULL,
                'remote_nid' => (int) $remote_nodes_array[$rnn]['nid'],
              );
              $comparsion_title_map[] = $remote_nodes_array[$rnn]['title'];
            }
          }
        }

      }
    }

    $comparsion_title_map = count($comparsion_title_map) > 0 ? array_unique($comparsion_title_map) : $comparsion_title_map;
    $local_unsinced_types = array_diff(array_keys($local_map), array_keys($remote_map));
    $remote_unsinced_types = array_diff(array_keys($remote_map), array_keys($local_map));

    if ($local_unsinced_types) {
      foreach ($local_unsinced_types as $unsinced_type) {
        foreach ($local_map[$unsinced_type] as $nid => $title) {
          if (!in_array($title, $comparsion_title_map)) {
            $comparsion_map[] = array(
              'title' => $title,
              'local_nid' => (int) $nid,
              'remote_nid' => NULL,
            );
            $comparsion_title_map[] = $title;
          }
        }
        unset($local_map[$unsinced_type]);
      }
    }

    if ($remote_unsinced_types) {
      foreach ($remote_unsinced_types as $unsinced_type) {
        foreach ($remote_map[$unsinced_type] as $nid => $title) {
          if (!in_array($title, $comparsion_title_map)) {
            $comparsion_map[] = array(
              'title' => $title,
              'local_nid' => NULL,
              'remote_nid' => (int) $nid,
            );
            $comparsion_title_map[] = $title;
          }
        }
        unset($remote_map[$unsinced_type]);
      }
    }

    foreach ($local_map as $type => $nodes) {
      if ($local_uniq_nodes = array_diff($nodes, $remote_map[$type])) {
        foreach ($local_uniq_nodes as $nid => $title) {
          if (!in_array($title, $comparsion_title_map)) {
            $comparsion_map[] = array(
              'title' => $title,
              'local_nid' => (int) $nid,
              'remote_nid' => NULL,
            );
            $comparsion_title_map[] = $title;
          }

          unset($nodes[$nid]);
        }
      }

      if ($remote_uniq_nodes = array_diff($remote_map[$type], $nodes)) {
        foreach ($remote_uniq_nodes as $nid => $title) {
          if (!in_array($title, $comparsion_title_map)) {
            $comparsion_map[] = array(
              'title' => $title,
              'local_nid' => NULL,
              'remote_nid' => (int) $nid,
            );
            $comparsion_title_map[] = $title;
          }

          unset($remote_map[$type][$nid]);
        }
      }

      foreach ($nodes as $nid => $title) {
        if (!is_array($remote_map[$type])) {
          continue;
        }
        if (in_array($title, $comparsion_title_map)) {
          continue;
        }

        $comparsion_map[] = array(
          'title' => $title,
          'local_nid' => $nid,
          'remote_nid' => array_search($title, $remote_map[$type]),
        );

        $comparsion_title_map[] = $title;
      }
    }

    db_truncate('ecs_reference_map')->execute();

    // Batch save of comparisons array to DB.
    $batch_info = ecs_batch_generate($comparsion_map);
    batch_set($batch_info);
    $batch = &batch_get();
    $batch['progressive'] = FALSE;
    batch_process('/admin/config/system/ecs');

    // Step 3 - save map remotely.
    $endpoint = $url . '/set_remote_map';
    $data = array(
      'map' => json_encode($comparsion_map),
      'checksum' => drupal_hmac_base64(variable_get('ecs_shared_secret', substr(md5(rand()), 0, 7)), variable_get('ecs_shared_secret', substr(md5(rand()), 0, 7))),
    );

    $options = array(
      'method' => 'POST',
      'data' => drupal_http_build_query($data),
      'timeout' => 15,
      'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
    );

    $remote_nodes = drupal_http_request(trim($endpoint), $options);

    drupal_set_message(t('Sync successfully completed.'), 'status');
    return TRUE;
  }

  drupal_set_message(t('An error occurred while syncing.'), 'error');
  return FALSE;
}

/**
 * Generate Batch process.
 *
 * @param array $comparsion_map
 *   Array of nodes which needed to be synced.
 * @param bool $remote
 *   Boolean value that determine this is remote site or not.
 *
 * @return array
 *   Batch process.
 */
function ecs_batch_generate(array $comparsion_map, $remote = FALSE) {
  set_time_limit(0);
  $operations = array();

  foreach ($comparsion_map as $comparsion) {
    $operations[] = array('ecs_compare_save', array($comparsion, $remote, 0));
  }

  $batch = array(
    'title' => t('Save comparsion array'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('An error occurred during processing'),
    'operations' => $operations,
    'finished' => 'ecs_batch_batch_finished',
  );

  return $batch;
}

/**
 * Batch callback: Performs actions when the compare batch is done.
 */
function ecs_batch_batch_finished($success, $results, $operations) {
  $message = '';

  if ($success) {
    $type = WATCHDOG_NOTICE;
    $message .= t('Comparions array saved successful');
  }
  else {
    $type = WATCHDOG_ERROR;
    $message = t('Finished with an error.');
  }

  drupal_set_message($message);
  watchdog('ecs', $message, array(), $type);
}

/**
 * Find nodes with same title but diffetent content.
 *
 * @param array $map_array
 *   Local content map aliases array.
 * @param bool $nidsonly
 *   Return only id of node, useful for data sync.
 *
 * @return array
 *   Array of dublicated entity.
 */
function ecs_find_map_duplicates(array $map_array, $nidsonly = FALSE) {
  $duplicates = $unique_nodes = array();

  foreach ($map_array as $type => $nodes) {
    $unique_nodes = array_unique($nodes);

    if (count($nodes) != count($unique_nodes)) {
      $duplicates[$type] = array_diff_assoc($nodes, $unique_nodes);
      $lostvalues = array_values(array_unique($duplicates[$type]));

      foreach ($lostvalues as $value) {
        $lostkey = array_search($value, $unique_nodes);
        $duplicates[$type][$lostkey] = $value;
      }
    }
  }

  if ($nidsonly) {
    $nids_to_send = array();

    foreach ($duplicates as $values) {
      $nids_to_send = array_merge($nids_to_send, array_keys($values));
    }

    $nids_to_send = array_unique($nids_to_send);
    $duplicates = $nids_to_send;
  }

  return $duplicates;
}

/**
 * Get nid of local node by remote node id.
 *
 * @param int $remote_node
 *   Remote node id.
 *
 * @return int|bool
 *   Local node id if exist otherwise FALSE
 */
function ecs_get_local_by_remote_unique($remote_node) {
  $remote = db_select('ecs_reference_map', 'map')
    ->fields('map', array('local_nid'))
    ->condition('map.remote_nid', $remote_node)
    ->execute();
  $remote = $remote->fetchObject();

  return (isset($remote->local_nid) && !empty($remote->local_nid)) ? $remote->local_nid : FALSE;
}

/**
 * Get alias id of local and remote node.
 *
 * @param int $local_nid
 *   Local node id.
 * @param int $remote_nid
 *   Remote node id.
 *
 * @return int
 *   Alias id.
 */
function ecs_get_reference_id($local_nid, $remote_nid) {
  if (!is_numeric($local_nid) || !is_numeric($remote_nid)) {
    return FALSE;
  }

  $db_refmap_query = db_select('ecs_reference_map', 'refmap')->fields('refmap', array('id'));
  $and = db_and();
  $and->condition('refmap.remote_nid', $remote_nid)->condition('refmap.local_nid', '', 'IS NULL');
  $or = db_or();
  $or->condition('refmap.local_nid', $local_nid)->condition($and);
  $result = $db_refmap_query->condition($or)->execute();

  return $result->fetchField();
}

/**
 * Update alias for node.
 *
 * @param int $map_id
 *   Unque id of alias.
 * @param array $comparsion
 *   Array of nodes data to update.
 * @param bool $remote
 *   Use for determine site whick make call.
 * @param int $last_sync
 *   Last sync time.
 */
function ecs_compare_update($map_id, array $comparsion, $remote = FALSE, $last_sync = 0) {
  $q = db_update('ecs_reference_map');

  if ($remote) {
    $q->fields(array(
      'local_nid' => $comparsion['remote_nid'],
      'remote_nid' => $comparsion['local_nid'],
      'title' => $comparsion['title'],
      'created' => time(),
      'last_sync' => $last_sync,
    ));
  }
  else {
    $q->fields(array(
      'local_nid' => $comparsion['local_nid'],
      'remote_nid' => $comparsion['remote_nid'],
      'title' => $comparsion['title'],
      'created' => time(),
      'last_sync' => $last_sync,
    ));
  }

  $q->condition('id', $map_id);
  $q->execute();
}

/**
 * Save alias for node.
 *
 * @param array $comparsion
 *   Array of nodes data to save.
 * @param bool $remote
 *   Use for determine site whick make call.
 * @param int $last_sync
 *   Last sync time.
 */
function ecs_compare_save(array $comparsion, $remote = FALSE, $last_sync = 0) {
  $insert_map = db_insert('ecs_reference_map');
  $insert_map->fields(
    array('local_nid', 'remote_nid', 'title', 'created', 'last_sync')
  );

  if ($remote) {
    $insert_map->values(array(
      'local_nid' => $comparsion['remote_nid'],
      'remote_nid' => $comparsion['local_nid'],
      'title' => $comparsion['title'],
      'created' => time(),
      'last_sync' => $last_sync,
    ));
  }
  else {
    $insert_map->values(array(
      'local_nid' => $comparsion['local_nid'],
      'remote_nid' => $comparsion['remote_nid'],
      'title' => $comparsion['title'],
      'created' => time(),
      'last_sync' => $last_sync,
    ));
  }

  $insert_map->execute();
}

/**
 * Load nodes by given array of nid.
 *
 * @param array $nids
 *   Array of nids.
 *
 * @return array
 *   Loaded nodes.
 */
function ecs_load_nodes_array($nids = array()) {
  $nodes = array();
  if (is_array($nids) && count($nids) > 0) {
    foreach ($nids as $nid) {
      $nodes[$nid] = node_load($nid);
    }
  }

  return $nodes;
}
