<?php
/**
 * @file
 * Definition of ECSSyncQueue class.
 */

/**
 * Class ECSSyncQueue. Queue the packets.
 */
class ECSSyncQueue {

  private $cronlimit;

  /**
   * Constructor.
   */
  public function __construct() {
    $this->cronlimit = 25;
  }

  /**
   * Process queue (on cron runs).
   */
  public function processQueue() {

    $queued_packets = db_select('ecs_packet_queue', 'epq')
      ->fields('epq', array(
        'id',
        'endpoint',
        'packet',
      ))
      ->orderBy('epq.id')
      ->range(0, $this->cronlimit)
      ->execute();

    $delete = array();

    while ($queued_packet = $queued_packets->fetchObject()) {
      $packet = new ECSSyncPacket();
      $packet->decode($queued_packet->packet);

      // Relay packet but do not queue.
      if (_ecs_relay_packet($queued_packet->endpoint, $packet, FALSE)) {
        $delete[] = $queued_packet->id;
      }
    }

    if (count($delete) > 0) {
      db_delete('ecs_packet_queue')
        ->condition('id', $delete, 'IN')
        ->execute();
    }

    // Update error count.
    db_update('ecs_packet_queue')
      ->expression('errors', 'errors + 1')
      ->execute();

    // Delete errors that have gone over the limit.
    db_delete('ecs_packet_queue')
      ->condition('errors', variable_get('ecs_error_limit', 5), '>')
      ->execute();
  }

  /**
   * Add the packet to queue.
   *
   * @param string $endpoint
   *   Host URL.
   * @param ECSSyncPacket $packet
   *   Packet to queue.
   */
  public function queuePacket($endpoint, ECSSyncPacket &$packet) {

    // Queueing disabled.
    if (variable_get('ecs_error_limit', 5) == 0) {
      return;
    }

    // Add obj to queue.
    db_insert('ecs_packet_queue')
      ->fields(array(
        'endpoint' => $endpoint,
        'queue_identifier' => $packet->getQueueIdentifier(),
        'packet' => $packet->encoded(),
      ))
      ->execute();
  }

  /**
   * Test if endpoint has queued packets.
   *
   * @param string $endpoint
   *   Host URL.
   *
   * @return bool
   *   FALSE if not TRUE if endpoint has queued packets.
   */
  public function endpointHasQueuedPackets($endpoint) {
    $endpoints = db_select('ecs_packet_queue', 'epq')
      ->fields('epq', array('endpoint'))
      ->condition('endpoint', $endpoint)
      ->range(0, 1)
      ->execute();
    return $endpoints->rowCount() > 0;
  }

}
