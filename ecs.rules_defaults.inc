<?php
/**
 * @file
 * ECS default rules configuration.
 */

/**
 * Define default rules configurations for ECS module.
 */
function ecs_default_rules_configuration() {

  $configs = array();
  $rule_sync = '{ "rules_sync_content" : {
      "LABEL" : "Sync content",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "ecs sync" ],
      "REQUIRES" : [ "rules", "ecs" ],
      "ON" : { "node_update" : [], "node_insert" : [] },
      "IF" : [
        { "ecs_note_type_condition" : { "node" : [ "node" ] } },
        { "NOT ecs_condition_invoked" : [] }
      ],
      "DO" : [
        { "ecs_sync" : {
            "entity" : [ "node" ],
            "unique" : [ "node:nid" ],
            "fields" : { "value" : { "-all-" : "-all-" } },
            "create" : "1",
            "endpoint" : "<?php print variable_get(\'ecs_remore_server\'); ?>"
          }
        }
      ]
    }
  }';

  $rule_delete = '{ "rules_sync_delete_content" : {
      "LABEL" : "Sync delete content",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "ecs sync" ],
      "REQUIRES" : [ "ecs", "rules" ],
      "ON" : { "node_delete" : [] },
      "IF" : [
        { "ecs_note_type_condition" : { "node" : [ "node" ] } },
        { "NOT ecs_condition_invoked" : [] }
      ],
      "DO" : [
        { "ecs_del" : {
            "entity" : [ "node" ],
            "unique" : [ "node:nid" ],
            "endpoint" : "<?php print variable_get(\'ecs_remore_server\'); ?>"
          }
        }
      ]
    }
  }';

  $configs['rules_sync_content'] = rules_import($rule_sync);
  $configs['rules_sync__delete_content'] = rules_import($rule_delete);
  return $configs;
}
