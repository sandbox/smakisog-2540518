<?php
/**
 * @file
 * ECS administration UI related functions.
 */

/**
 * ECS administration form.
 */
function ecs_admin_form() {
  $form = array();

  $form['content_sync_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => FALSE,
  );
  $form['content_sync_settings']['ecs_sync_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Choose node types to sync'),
    '#options' => node_type_get_names(),
    '#default_value' => variable_get('ecs_sync_node_types', array()),
  );
  $form['content_sync_settings']['ecs_remore_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Set remote server path'),
    '#default_value' => variable_get('ecs_remore_server', array()),
    '#description' => t('Set the host name with endpoint. E.g. http://www.example.com/ecs'),
    '#required' => TRUE,
  );
  $form['content_sync_settings']['content_sync_control'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sync content'),
    '#collapsible' => FALSE,
  );
  $form['content_sync_settings']['content_sync_control']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Create Alias Map'),
    '#submit' => array('ecs_admin_nodes_sync'),
    '#validate' => array('ecs_admin_form_validate'),
    '#suffix' => '<p>' . t('Manually starts sync/re-sync procces between sites. Required for automatic content syncing.') . '</p>',
  );

  $form['content_sync_settings']['security'] = array(
    '#type' => 'fieldset',
    '#title' => t('Security'),
    '#collapsible' => FALSE,
  );
  $form['content_sync_settings']['security']['ecs_shared_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Shared secret between sites'),
    '#description' => t('This is the shared secret between synchronizing Drupal sites. This must be the same in synchronized sites.'),
    '#size' => 128,
    '#required' => TRUE,
    '#default_value' => variable_get('ecs_shared_secret', substr(md5(rand()), 0, 7)),
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * ECS settings form submit handler.
 */
function ecs_admin_form_submit($form_id, $form_state) {
  variable_set('ecs_shared_secret', $form_state['values']['ecs_shared_secret']);

  if (!isset($form_state['values']['ecs_error_limit']) || empty($form_state['values']['ecs_error_limit'])) {
    variable_set('ecs_error_limit', 0);
  }
  else {
    variable_set('ecs_error_limit', $form_state['values']['ecs_error_limit']);
  }

  variable_set('ecs_sync_node_types', $form_state['values']['ecs_sync_node_types']);
  variable_set('ecs_remore_server', $form_state['values']['ecs_remore_server']);

  // Call instance create function.
  ecs_instances_install();
}

/**
 * Additinal submit for admin form.
 */
function ecs_admin_nodes_sync($form_id, $form_state) {
  if (!variable_get('ecs_remore_server')) {
    drupal_set_message(t('Please, provide valid remote server address.'), 'error');
    return FALSE;
  }
  if (!variable_get('ecs_sync_node_types')) {
    drupal_set_message(t('Please, select at last one content type to sync.'), 'error');
    return FALSE;
  }

  require_once drupal_get_path('module', 'ecs') . '/includes/ecs.helper.inc';
  ecs_compare_content();
}

/**
 * Implemets hook_form_validates().
 */
function ecs_admin_form_validate($form, &$form_state) {
  $endpoint = $form_state['values']['ecs_remore_server'] . '/content_mapping';
  $data = array(
    'checksum' => drupal_hmac_base64($form_state['values']['ecs_shared_secret'], $form_state['values']['ecs_shared_secret']),
  );

  $options = array(
    'method' => 'POST',
    'data' => drupal_http_build_query($data),
    'timeout' => 15,
    'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
  );
  $remote_map_request = drupal_http_request(trim($endpoint), $options);

  if (isset($remote_map_request->code) && $remote_map_request->code != '200') {
    form_set_error('ecs_remore_server', t("Request couldn't reach provided URL. Please, check URL or/and Shared Secret word ant try again."));
    return FALSE;
  }

  return TRUE;
}
