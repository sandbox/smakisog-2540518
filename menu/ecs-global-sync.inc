<?php
/**
 * @file
 * ECS global sync related functions.
 */

/**
 * ECS global sync callback.
 */
function ecs_globalsync_form($form, &$form_state) {
  $form = array();
  $form['import'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sync content'),
  );
  $form['import']['message'] = array(
    '#type' => 'item',
    '#markup' => '<div class="messages warning">' . t("Important! Do not close this window while sync process is running.") . '</div>',
  );

  $form['import']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Run sync process'),
  );
  return $form;
}

/**
 * Run sync.
 */
function ecs_globalsync_form_submit($form, &$form_state) {
  $batch_info = ecs_globalsync_form_batch_generate();
  batch_set($batch_info);
}

/**
 * Generate batch process.
 *
 * @return array
 *   Batch process.
 */
function ecs_globalsync_form_batch_generate() {
  $operations = array();
  $sync_types = variable_get('ecs_sync_node_types', array());
  $sync_types = array_flip($sync_types);

  // Get related maps.
  $map = db_select('ecs_reference_map', 'map')
    ->fields('map', array('local_nid', 'remote_nid'))
    ->execute()->fetchAll();

  $ids_array = array();

  foreach ($map as $multiple_ids) {
    $ids_array[] = $multiple_ids->local_nid;
  }

  $nodes = node_load_multiple($ids_array);

  foreach ($map as $ids) {
    // Skip ids if one of id have artifact values or local node not exist.
    if ($ids->local_nid != 0 || !is_null($ids->local_nid)) {
      // If we don't sync this content types then break.
      if (isset($nodes[$ids->local_nid])) {
        $node = $nodes[$ids->local_nid];

        if (in_array($node->type, $sync_types)) {
          $operations[] = array(
            'ecs_globalsync',
            array($ids->local_nid, $ids->remote_nid),
          );
        }
      }

    }
  }

  $batch = array(
    'title' => t('Importing content'),
    'operations' => $operations,
    'finished' => 'ecs_globalsync_finished',
    'file' => drupal_get_path('module', 'ecs') . '/menu/ecs-global-sync.inc',
  );

  return $batch;
}

/**
 * Init finish process behavior.
 */
function ecs_globalsync_finished($success, $results, $operations) {
  if ($success) {
    $type = WATCHDOG_NOTICE;
    $message = t('Content updated.');
  }
  else {
    $type = WATCHDOG_ERROR;
    $message = t('Finished with an error.');
  }

  drupal_set_message($message);
  watchdog('ecs', $message, array(), $type);
}

/**
 * Syncronize nodes one by one and run rule if needed.
 *
 * @param int $local_nid
 *   Local node id.
 * @param int $remote_nid
 *   Remote node id.
 * @param mixed &$context
 *   Context.
 *
 * @return bool
 *   TRUE if sync is success else FALSE
 */
function ecs_globalsync($local_nid, $remote_nid, &$context) {
  require_once drupal_get_path('module', 'ecs') . '/ecs.rules.inc';
  $url = variable_get('ecs_remore_server', 'http://');

  if (!is_numeric($local_nid) || !is_numeric($remote_nid)) {
    if (is_null($remote_nid)) {
      // Create remote entity.
      $node = node_load($local_nid);

      $entity = entity_metadata_wrapper('node', $node);
      $create = 1;
      $endpoint = $url . '/ecs';
      $fields['-all-'] = '-all-';

      ecs_rules_sync($entity, $entity->nid, $fields, $create, $endpoint);
      $context['results'][] = $entity->nid->value() . ' : ' . check_plain($entity->title->value());
      $context['message'] = t('Create: @create', array('@create' => $entity->title->value()));

      return TRUE;
    }

    return FALSE;
  }

  $endpoint = $url . '/get_remote_nodes';
  $data = array(
    'nids' => array($remote_nid),
    'checksum' => drupal_hmac_base64(variable_get('ecs_shared_secret', substr(md5(rand()), 0, 7)), variable_get('ecs_shared_secret', substr(md5(rand()), 0, 7))),
  );

  $options = array(
    'method' => 'POST',
    'data' => drupal_http_build_query($data),
    'timeout' => 15,
    'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
  );

  $remote_nodes = drupal_http_request(trim($endpoint), $options);
  if (!isset($remote_nodes->data) || $remote_nodes->status_message != 'OK') {
    $context['message'] = t('Warning: @warning', array('@warning' => $remote_nodes->status_message));
    return TRUE;
  }

  $remote_node = (object) array_shift(json_decode($remote_nodes->data, 1));
  if (ecs_node_compare($local_nid, $remote_node)) {
    $context['message'] = t('Equal: @local - @remote', array('@local' => $local_nid, '@remote' => $remote_node->title));
    return TRUE;
  }

  // Sync the nodes.
  $node = node_load($local_nid);

  $entity = entity_metadata_wrapper('node', $node);
  $create = 0;
  $endpoint = $url . '/ecs';
  $fields['-all-'] = '-all-';

  ecs_rules_sync($entity, $entity->nid, $fields, $create, $endpoint);

  $context['results'][] = $entity->nid->value() . ' : ' . check_plain($entity->title->value());
  $context['message'] = t('Update: @update', array('@update' => $entity->title->value()));

  return TRUE;

}
