<?php
/**
 * @file
 * ECS callback related functions.
 */

/**
 * ECS update callback.
 */
function ecs_sync_callback() {

  require_once drupal_get_path('module', 'ecs') . '/ecs.inc';

  $entity_sync_packet = new ECSSyncPacket();
  // Process and validate data.
  if (!$entity_sync_packet->decode(file_get_contents('php://input'))) {
    drupal_add_http_header('Status', '403 Forbidden');
    drupal_exit();
  }

  drupal_alter('ecs_receive', $entity_sync_packet);

  // Fetch the entity.
  $entity = $entity_sync_packet->fetchEntity();

  // No entity -> fail.
  if (!$entity) {
    drupal_add_http_header('Status', '409 Conflict');
    drupal_exit();
  }

  $field_handlers = ecs_get_field_handlers();

  switch ($entity_sync_packet->op) {
    case 'del':
      // Delete, but only try to delete existing entities.
      if (!isset($entity->is_new)) {
        require_once drupal_get_path('module', 'ecs') . '/includes/ecs.helper.inc';

        $id = ecs_get_local_by_remote_unique($entity_sync_packet->uniqueValue);
        entity_delete($entity_sync_packet->entityType, $id);
      }
      break;

    case 'sync':
      require_once drupal_get_path('module', 'ecs') . '/includes/ecs.helper.inc';

      $local_nid = ecs_get_local_by_remote_unique($entity_sync_packet->uniqueValue);

      if ($local_nid != FALSE) {
        $entity->is_new = FALSE;
      }

      if ($entity_sync_packet->create && $entity->is_new) {
        // Create new entity.
        foreach ($entity_sync_packet->fields as $field_name => $field_data) {
          $info = field_info_field($field_name);

          if (isset($field_handlers[$info['type']])) {
            $entity->{$field_name} = $field_handlers[$info['type']]['in']($entity, $field_name, $field_data);
          }
          else {
            $entity->{$field_name} = $field_data;
          }
        }

        $wrapper = entity_metadata_wrapper($entity_sync_packet->entityType, $entity);
        $wrapper->author->set(1);
        $wrapper->language->set(LANGUAGE_NONE);
        $wrapper->save();

        ecs_synced_map_save($wrapper->getIdentifier(), $entity_sync_packet->uniqueValue, $entity->title);
        break;
      }
      else {
        // Update entity fields.
        $entity = entity_load_single($entity_sync_packet->entityType, $local_nid);

        foreach ($entity_sync_packet->fields as $field_name => $field_data) {
          $info = field_info_field($field_name);
          if (isset($field_handlers[$info['type']])) {
            $entity->{$field_name} = $field_handlers[$info['type']]['in']($entity, $field_name, $field_data);
          }
          else {
            $entity->{$field_name} = $field_data;
          }
        }

        $wrapper = entity_metadata_wrapper($entity_sync_packet->entityType, $entity);
        $wrapper->save();

        ecs_synced_map_save($wrapper->getIdentifier(), $entity_sync_packet->uniqueValue, $entity->title);
        break;
      }
  }

  drupal_exit();
}

/**
 * Synchronize aliase map.
 *
 * @param int $local_nid
 *   Local node id.
 * @param int $remote_nid
 *   Remote node id.
 * @param string $title
 *   Node title.
 */
function ecs_synced_map_save($local_nid, $remote_nid, $title) {
  $url = variable_get('ecs_remore_server', 'http://');
  $endpoint = $url . '/set_remote_single_map';

  $map = array(
    'local_nid' => $local_nid,
    'remote_nid' => $remote_nid,
    'title' => $title,
  );

  $data = array(
    'map' => $map,
    'checksum' => drupal_hmac_base64(variable_get('ecs_shared_secret', substr(md5(rand()), 0, 7)), variable_get('ecs_shared_secret', substr(md5(rand()), 0, 7))),
  );

  $options = array(
    'method' => 'POST',
    'data' => drupal_http_build_query($data),
    'timeout' => 15,
    // 'headers' => array('Content-Type' => 'application/json'),
    'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
  );

  $remote_set = drupal_http_request(trim($endpoint), $options);

  if ($remote_set->status_message == 'OK') {
    require_once drupal_get_path('module', 'ecs') . '/includes/ecs.helper.inc';
    $timestamp = time();

    if ($map_id = ecs_get_reference_id($map['local_nid'], $map['remote_nid'])) {
      ecs_compare_update($map_id, $map, FALSE, $timestamp);
    }
    else {
      ecs_compare_save($map, FALSE, $timestamp);
    }
  }
}
