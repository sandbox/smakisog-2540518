<?php
/**
 * @file
 * ECS conent mapping callback related functions.
 */

/**
 * ECS get conetnt mapping callback.
 */
function ecs_content_mapping_callback() {
  require_once drupal_get_path('module', 'ecs') . '/includes/ecs.helper.inc';
  $array_map = ecs_create_array();

  echo json_encode($array_map);
}

/**
 * ECS get conetnt by nid array callback.
 */
function ecs_get_remote_nodes_callback() {
  $nids = $_POST['nids'];

  if (is_array($nids)) {
    require_once drupal_get_path('module', 'ecs') . '/includes/ecs.helper.inc';
    $nodes = ecs_load_nodes_array($nids);

    echo json_encode($nodes);
    drupal_exit();
  }

  echo 'Invalid nids array parameters';
}

/**
 * Save master map array to child.
 */
function ecs_set_remote_map_callback() {
  $map = json_decode($_POST['map'], 1);

  if (is_array($map) && count($map) > 0) {
    db_truncate('ecs_reference_map')->execute();

    // Batch save of comparisons array to DB.
    require_once drupal_get_path('module', 'ecs') . '/includes/ecs.helper.inc';

    $batch_info = ecs_batch_generate($map, TRUE);

    batch_set($batch_info);
    $batch = &batch_get();
    $batch['progressive'] = FALSE;
    batch_process('');
  }
}

/**
 * Setup remote alias map.
 */
function ecs_set_remote_single_map_callback() {
  $map = $_POST['map'];

  if (is_array($map)) {
    require_once drupal_get_path('module', 'ecs') . '/includes/ecs.helper.inc';
    $timestamp = time();

    if ($map_id = ecs_get_reference_id($map['remote_nid'], $map['local_nid'])) {
      ecs_compare_update($map_id, $map, TRUE, $timestamp);
    }
    else {
      ecs_compare_save($map, TRUE, $timestamp);
    }
  }
}
